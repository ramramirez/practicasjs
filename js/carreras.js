const btnGenerar = document.getElementById('btnGenerar');

var apuesta = 0;

var ganador = '';

Gizmo.addEventListener('click', () => {
    Gizmo.style.backgroundColor = 'darkblue';
    Checo.style.backgroundColor = '#ac1639';
    Rayo.style.backgroundColor = '#ac1639';
    Meteoro.style.backgroundColor = '#ac1639';
    apuesta = 1;

    piloto1.style.border = '2px solid darkblue';
    piloto1.style.borderRadius = '35px';
    piloto2.style.border = '';
    piloto2.style.borderRadius = '';
    piloto3.style.border = '';
    piloto3.style.borderRadius = '';
    piloto4.style.border = '';
    piloto4.style.borderRadius = '';
    
});

Checo.addEventListener('click', () => {
    Gizmo.style.backgroundColor = '#ac1639';
    Checo.style.backgroundColor = 'darkblue';
    Rayo.style.backgroundColor = '#ac1639';
    Meteoro.style.backgroundColor = '#ac1639';
    
    apuesta = 2;

    piloto1.style.border = '';
    piloto1.style.borderRadius = '';
    piloto2.style.border = '2px solid darkblue';
    piloto2.style.borderRadius = '35px';
    piloto3.style.border = '';
    piloto3.style.borderRadius = '';
    piloto4.style.border = '';
    piloto4.style.borderRadius = '';

});

Rayo.addEventListener('click', () => {
    Gizmo.style.backgroundColor = '#ac1639';
    Checo.style.backgroundColor = '#ac1639';
    Rayo.style.backgroundColor = 'darkblue';
    Meteoro.style.backgroundColor = '#ac1639';

    apuesta = 3;


    piloto1.style.border = '';
    piloto1.style.borderRadius = '';
    piloto2.style.border = '';
    piloto2.style.borderRadius = '';
    piloto3.style.border = '2px solid darkblue';
    piloto3.style.borderRadius = '35px';
    piloto4.style.border = '';
    piloto4.style.borderRadius = '';

});

Meteoro.addEventListener('click', () => {
    Gizmo.style.backgroundColor = '#ac1639';
    Checo.style.backgroundColor = '#ac1639';
    Rayo.style.backgroundColor = '#ac1639';
    Meteoro.style.backgroundColor = 'darkblue';
 
    apuesta = 4;

    piloto1.style.border = '';
    piloto1.style.borderRadius = '';
    piloto2.style.border = '';
    piloto2.style.borderRadius = '';
    piloto3.style.border = '';
    piloto3.style.borderRadius = '';
    piloto4.style.border = '2px solid darkblue';
    piloto4.style.borderRadius = '35px';

});

/*------------------------------------------*/

btnPlay.addEventListener('click',function generar(){

    function azar(){
        pos = Math.floor(Math.random() * 4)+1;
        return pos;
    }
    
    meta = azar();
    
    if (meta == 1){
        ganador = 'Gizmo'
        piloto1.style.top = '40px';
        piloto1.style.transition = 'top 2s'
        piloto2.style.top = '40px';
        piloto2.style.transition = 'top 3s'
        piloto3.style.top = '40px';
        piloto3.style.transition = 'top 4s'
        piloto4.style.top = '40px';
        piloto4.style.transition = 'top 5s'
    }
    
    if (meta == 2){
        ganador = 'Checo'
        piloto2.style.top = '40px';
        piloto2.style.transition = 'top 2s'
        piloto1.style.top = '40px';
        piloto1.style.transition = 'top 4s'
        piloto3.style.top = '40px';
        piloto3.style.transition = 'top 4s'
        piloto4.style.top = '40px';
        piloto4.style.transition = 'top 3s'
    }
    
    if (meta == 3){
        ganador = 'Rayo'
        piloto3.style.top = '40px';
        piloto3.style.transition = 'top 2s'
        piloto1.style.top = '40px';
        piloto1.style.transition = 'top 3s'
        piloto2.style.top = '40px';
        piloto2.style.transition = 'top 4s'
        piloto4.style.top = '40px';
        piloto4.style.transition = 'top 4s'
    }
    
    if (meta == 4){
        ganador = 'Meteoro'
        piloto4.style.top = '40px';
        piloto4.style.transition = 'top 2s'
        piloto1.style.top = '40px';
        piloto1.style.transition = 'top 3s'
        piloto2.style.top = '40px';
        piloto2.style.transition = 'top 3s'
        piloto3.style.top = '40px';
        piloto3.style.transition = 'top 4s'
    }

    console.log("Apuesta:"+apuesta+"\nMeta:"+meta);

    if (apuesta == meta){
        document.getElementById('mostrarganador').setAttribute("value","El ganador fué: "+ganador+" ¡ACERTASTE!");   
        mostrarganador.style.backgroundColor = 'darkblue';
        mostrarganador.style.color = 'white';
    }


    if (apuesta == 0){
        document.getElementById('mostrarganador').setAttribute("value","El ganador fué: "+ganador);
        mostrarganador.style.backgroundColor = '#f9f9f9';
        mostrarganador.style.color = 'black';
    }

    if (apuesta != meta){
        document.getElementById('mostrarganador').setAttribute("value","El ganador fué: "+ganador);
        mostrarganador.style.backgroundColor = '#f9f9f9';
        mostrarganador.style.color = 'black';
    }

});

btnReset.addEventListener('click',function limpiar(){
    meta = 0;
    piloto1.style.top = '940px';
    piloto2.style.top = '940px';
    piloto3.style.top = '945px';
    piloto4.style.top = '940px';

    piloto1.style.transition = 'top 1s'
    piloto2.style.transition = 'top 1s'
    piloto3.style.transition = 'top 1s'
    piloto4.style.transition = 'top 1s'

    mostrarganador.style.backgroundColor = '#f9f9f9';
    mostrarganador.style.color = 'black';

});

btnClear.addEventListener('click',function limpiar(){

    document.getElementById('mostrarganador').setAttribute("value"," ");

    Gizmo.style.backgroundColor = '#ac1639';
    Checo.style.backgroundColor = '#ac1639';
    Rayo.style.backgroundColor = '#ac1639';
    Meteoro.style.backgroundColor = '#ac1639';
    apuesta = 0;
    meta = 0;
    piloto1.style.top = '940px';
    piloto2.style.top = '940px';
    piloto3.style.top = '945px';
    piloto4.style.top = '940px';

    piloto1.style.transition = 'top 1s'
    piloto2.style.transition = 'top 1s'
    piloto3.style.transition = 'top 1s'
    piloto4.style.transition = 'top 1s'


    piloto1.style.border = '';
    piloto1.style.borderRadius = '';
    piloto2.style.border = '';
    piloto2.style.borderRadius = '';
    piloto3.style.border = '';
    piloto3.style.borderRadius = '';
    piloto4.style.border = '';
    piloto4.style.borderRadius = '';

    mostrarganador.style.backgroundColor = '#f9f9f9';
    mostrarganador.style.color = 'black';

});